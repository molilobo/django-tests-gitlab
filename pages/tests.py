from django.test import TestCase
from django.test import Client

from .views import Counter

class CounterTests(TestCase):

    def test_creation(self):
        """Creation works"""
        counter = Counter()
        self.assertEqual(counter.count, 0)

    def test_increment(self):
        """Increment works"""
        counter = Counter()
        incremented = counter.increment()
        self.assertEqual(incremented, 1)



    def test_Not_None(self):
        counter = Counter()
        incremented = counter.increment()
        self.assertIsNotNone(incremented)




class GetTests (TestCase):

    def test_root(self):
        """Check root resource"""

        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h1>Django CMS</h1>', content)

    def test_empty(self):
        """Empty resource"""

        c = Client()
        response = c.get('/empty')
        self.assertEqual(response.status_code, 404)
        content = response.content.decode('utf-8')
        self.assertIn('<a href="/">Main page</a>', content)

    def test_create(self):
        """Empty resource"""

        item_content = "This is the content for item."
        c = Client()

        response = c.post('/item', {'content': item_content})
        # Verifica que el POST devuelva 200 OK
        self.assertEqual(response.status_code, 200)
        response = c.get('/item')
        # Verifica que el Get devuelva 200 OK
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')

        self.assertIn(item_content, content)#Verifica que el contenido creado este en la respuesta del GET
        self.assertIn('<form action="/item" method="post">', content)

        # self.assertEqual Sirve para verificar que dos elementos son iguales
        # self.assertIn verificar que un elemento está contenido dentro de otro elemento
        # ASERTS MAS COMUNES Y UTILES
        # assertNotEqual(a, b) - Verifica que a != b
        # assertTrue(x) - Verifica que x es True
        # assertFalse(x) - Verifica que x es False
        # assertNotIn(a, b) - Verifica que a no está en b
        # assertIs(a, b) - Verifica que a es b (compara objetos)
        # assertIsNot(a, b) - Verifica que a no es b
        # assertIsNone(x) - Verifica que x es None
        # assertIsNotNone(x) - Verifica que x no es
        #
