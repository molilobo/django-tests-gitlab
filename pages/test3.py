from django.test import TestCase
from django.test import Client

from .views import Counter

class GetTests(TestCase):
    def test_empty_con_otros_asserts(self):
        """Prueba un recurso vacío"""

        c = Client()
        response = c.get('/empty')

        # Verifica código 404
        self.assertEqual(response.status_code, 404)

        # Verifica que sea un mensaje de "Not Found"
        self.assertIn('Not Found', response.reason_phrase)

        content = response.content.decode('utf-8')

        # Verifica link a página principal
        self.assertIn('<a href="/">Main page</a>', content)

        # Verifica título con mensaje de error
        self.assertIn('<title>Page Not Found</title>', content)