from django.test import TestCase
from django.test import Client

from .views import Counter


class CounterTests(TestCase):

    def test_creation(self):
        """Creation works"""
        counter = Counter()
        self.assertEqual(counter.count, 0)

    def test_increment(self):
        """Increment works"""
        counter = Counter()
        incremented = counter.increment()
        self.assertEqual(incremented, 1)

    def test_Not_None(self):
        counter = Counter()
        incremented = counter.increment()
        self.assertIsNotNone(incremented)

        # self.assertEqual Sirve para verificar que dos elementos son iguales
        # self.assertIn verificar que un elemento está contenido dentro de otro elemento
        # ASERTS MAS COMUNES Y UTILES
        # assertNotEqual(a, b) - Verifica que a != b
        # assertTrue(x) - Verifica que x es True
        # assertFalse(x) - Verifica que x es False
        # assertNotIn(a, b) - Verifica que a no está en b
        # assertIs(a, b) - Verifica que a es b (compara objetos)
        # assertIsNot(a, b) - Verifica que a no es b
        # assertIsNone(x) - Verifica que x es None
        # assertIsNotNone(x) - Verifica que x no es
        #
